#!/bin/bash

# lister les conterur en cours
#asupprine=$(docker ps -a |  awk  'FNR>1 { print $1}')
#echo $asupprine

## Variables ####################################################

##DIR="${HOME}/generator"
USER_SCRIPT=${USER}

## Functions ####################################################

help(){

echo "USAGE :

  ${0##*/} [-h] [--help]

  Options :

    -h, --help : aides

    -al, --arrell : arrete  les conterneurs
    -sl, --supprimel : suprime les  conterneurs locaux
    
    -ad, --arreted : arrete  conterneurs distant
    -sd, --supprimed : suprime les  conterneurs distant

    -i, --ip : affichage des ip
    -dl, --demarer: demarrage de conteneur registry sur jenkins

"
}

ip() {
for i in $(docker ps -q); do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;done
}


parser_options(){

case $@ in
        -h|--help)
          help
        ;;
        -al|--arretel)
          arretel
        ;;
	-sl|--suprimel)
          suprimel
        ;;
	-ad|--arreted)
           arreted
        ;;
	-sd|--suprimed)
          suprimed
        ;;
	-dl|--registry)
	 registry 
	;;
        *)
        echo "option invalide, lancez -h ou --help"
esac
}

arretel(){
echo "arrete les conteneurx locaux"
for i in $(docker ps |  awk  'FNR>1 { print $1}'); do
 docker stop $i 2> \dev\null;done

}
suprimel(){
echo "supprime les conteneurs locaux"
for i in $(docker ps -a |  awk  'FNR>1 { print $1}'); do 
docker rm $i;done

}

arreted(){
echo "arreteles conteuenr distant"
ssh 192.168.43.154 ./arret_cont.sh
}

suprimed(){
echo "supprime les conteneurs distant"
ssh 192.168.43.154 ./supp_cont.sh
}
registry(){
docker run -d -p 5000:5000 \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/server.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/server.key \
-v /etc/pki/tls/certs:/certs \
-v /var/lib/docker/registry:/var/lib/registry \
registry:2
}

## Execute ######################################################

parser_options $@
#ip
